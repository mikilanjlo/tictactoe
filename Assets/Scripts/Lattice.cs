﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lattice : MonoBehaviour
{
    [SerializeField] public int LatticeId;
    public void ButtonClick(int cell)
    {
        GameManager.instance.ButtonClick(LatticeId, cell);
    }
}
