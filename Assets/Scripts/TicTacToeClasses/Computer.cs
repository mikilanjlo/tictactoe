﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class Computer : AGamer
{

    private int levelPercent = 100 - (int)(UnityEngine.Random.value * 25);
    private Timer m_timer;

    public override void NewTurn()
    {
        base.NewTurn();
        //m_timer = new Timer(2000);
        //m_timer.Elapsed += TurnTimer;
        //m_timer.AutoReset = false;
        //m_timer.Enabled = true;
        //System.Threading.Thread.Sleep(2000);
        Turn();
        //StartCoroutine(TurnCoroutine());
    }

    private void TurnTimer(object source, ElapsedEventArgs e)
    {
        //yield return new WaitForSeconds(2f);
        Turn();
    }

    public override void Turn()
    {

        //ABoard newboard = (ABoard)board.Clone();
        //Dictionary<int, double> ratings = new Dictionary<int, double>();
        ////for (int i = 0; i < m_rows; i++)
        ////{
        ////    for (int j = 0; j < m_mas.Length / m_rows; j++)
        ////    {
        ////        if (newMas[i, j] == null)
        ////        {
        ////            newMas[i, j] = ETicOrTac.O;
        ////            ratings.Add(i * m_rows + j, SimulateTurn(newMas, ETicOrTac.X));
        ////            newMas[i, j] = null;
        ////        }
        ////    }
        ////}
        //for(int i = newboard.MinCells; i < newboard.MaxCells; i++)
        //{
        //    if (newboard.IsFreePlace(i))
        //    {
        //        newboard.Turn(Symbol, i);
        //        ratings.Add(i, SimulateTurn(newboard, Symbol.InverseETicOrTac()));
        //        newboard.ReturnTurn();
        //    }
        //}

        //double max = double.MinValue;
        //int ind = 0;
        //foreach (KeyValuePair<int, double> keyValue in ratings)
        //{
        //    if (max < keyValue.Value)
        //    {
        //        max = keyValue.Value;
        //        ind = keyValue.Key;
        //    }
        //}
        //double num = UnityEngine.Random.value;
        ////double num2 = (levelPercent / 100);
        ////Debug.Log(num);
        //ind =  (num < (levelPercent / 100f) ? ind : RandomFreePlace()) ;
        int ind = board.BestVariant(Symbol);
        board.Turn(Symbol, ind );
        base.Turn(ind);
    }


    //private double SimulateTurn(ABoard newboard, ETicOrTac symbol)
    //{
        
    //    double result = 0;
    //    if (WinComputerResults(newboard) == null)
    //    {

    //        for (int i = newboard.MinCells; i < newboard.MaxCells; i++)
    //        {
    //            if (newboard.IsFreePlace(i))
    //            {
    //                newboard.Turn(symbol, i);
    //                result += SimulateTurn(newboard, symbol.InverseETicOrTac()) * (newboard.FreePlacesCount + 1);
    //                newboard.ReturnTurn();
    //            }
    //        }
    //        return result;
    //    }
    //    else
    //        return result + (int)WinComputerResults(newboard);
    //}

    private class RecursTurn
    {
        private int ind;
        private ETicOrTac symbol;

        public int Ind { get => ind; set => ind = value; }
        public ETicOrTac Symbol { get => symbol; set => symbol = value; }
    }

    //private double SimulateTurn(ABoard newboard, ETicOrTac symbol)
    //{
    //    int loop = 0;
    //    double result = 0;
    //    List<RecursTurn> recursTurns = new List<RecursTurn>();
    //    recursTurns.Add(new RecursTurn() { Ind = 0, Symbol = symbol });
    //    while(recursTurns.Count > 0)
    //    {
    //        if(WinComputerResults(newboard) != null)
    //        {
    //            result += (int)WinComputerResults(newboard) * (newboard.FreePlacesCount + 1);
    //            recursTurns.RemoveAt(recursTurns.Count - 1);
    //            newboard.ReturnTurn();
    //        }
    //        else
    //        {
    //            RecursTurn recursTurn = recursTurns[recursTurns.Count - 1];
    //            if (recursTurn.Ind >= newboard.MaxCells)
    //            {
    //                recursTurns.RemoveAt(recursTurns.Count - 1);
    //                newboard.ReturnTurn();
    //                continue;
    //            }
    //            if (newboard.IsFreePlace(recursTurn.Ind))
    //            {
    //                newboard.Turn(recursTurn.Symbol, recursTurn.Ind);
    //                recursTurns.Add(new RecursTurn() { Ind = 0, Symbol = recursTurns[recursTurns.Count - 1].Symbol.InverseETicOrTac()});
    //            }
    //            recursTurn.Ind++;
    //        }
    //        loop++;
    //        if (loop > 10000)
    //            throw new StackOverflowException("loop infinity");
    //    }
    //    return result;
    //}

    private int? WinComputerResults(ABoard newboard)
    {
        
        if (newboard.IsFinal(Symbol.InverseETicOrTac()))
        {
            return -1;
        }
        else
        {
            if (newboard.IsFinal(Symbol))
            {
                return 1;
            }
            else if (newboard.IsFool) { return 0; }
        }
        return null;
    }

    private int RandomFreePlace()
    {
        List<int> masIndexes = new List<int>();
        for (int i = board.MinCells; i < board.MaxCells; i++)
        {
            if (board.IsFreePlace(i))
            {
                masIndexes.Add(i);
            }
        }
        return masIndexes[(int)(UnityEngine.Random.value * masIndexes.Count)];
    }
}
