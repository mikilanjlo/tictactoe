﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Assets.Scripts.TicTacToeClasses;
using UnityEngine;

public class Board2D : ABoard
{
    private readonly int[,] lin = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 1, 4, 7 }, { 2, 5, 8 }, { 3, 6, 9 }, { 1, 5, 9 }, { 3, 5, 7 } };
    private static int m_rows = 3;
    private static int m_columns = 3;
    private Mas2d[,] m_mas = Mas2d.GetNewDoubleArray(Rows, Columns);

    public delegate void DelegeteTutnEvent(int cell, ETicOrTac sumbol);
    public event DelegeteTutnEvent BigTurn;


    public override int MinCells => 0;

    public override int MaxCells => Rows*Columns*9;

    public override int FreePlacesCount
    {
        get
        {
            //int count = 0;
            //foreach (ETicOrTac? symbol in m_mas)
            //    if (symbol == null)
            //        count++;
            //return count;
            int count = 0;
            foreach (Mas2d mas2D in m_mas)
                if (mas2D.value == null)
                    count += mas2D.ticTacToe.FreePlacesCount;
            return count;
        }
    }

    public override bool IsFool
    {
        get
        {
            //foreach (Mas2d? e in m_mas)
            //    if (e == null)
            //        return false;
            //return true;
            foreach(Mas2d e in m_mas)
            {
                if (e.value == null)
                    return false;
            }
            return true;
        }
    }

    public static int Rows { get => m_rows;  }
    public static int Columns { get => m_columns;  }

    public Board2D(AGamer firstGamer, AGamer secondGamer) :base(firstGamer, secondGamer) { }
    public Board2D(Mas2d[,] array) : base()
    {
        m_mas = array;
    }

    public override void Clear()
    {
        m_mas = Mas2d.GetNewDoubleArray(Rows, Columns);
    }

    public override object Clone()
    {
        Mas2d[,] array = new Mas2d[Rows, Columns];
        for (int i = 0; i < Rows; i++)
            for (int j = 0; j < Columns; j++)
            {
                array[i, j] = new Mas2d(m_mas[i, j].value, (SimpleBoard)m_mas[i, j].ticTacToe.Clone(), m_mas[i, j].IsClose);
                
            }
        return new Board2D(array);
    }

    public override void Turn(ETicOrTac symbol, int number)
    {
        if (IsFreePlace(number))
        {
            Mas2d cell = m_mas[number / 9 / m_rows, number / 9 % m_columns];
            cell.ticTacToe.Turn(symbol,number % 9);
            if (cell.ticTacToe.IsFinal(symbol)) {
                cell.value = symbol;
                BigTurn?.Invoke(number / 9,symbol);
              
            }
            else
            {
                if (cell.ticTacToe.IsFool)
                    cell.value = ETicOrTac.Draw;
            }
            if (m_mas[(number % 9) / Rows, (number % 9) % Columns].value == null)
            {
                CloseAllBigCells(number % 9 / Rows, number % 9 % Columns);
            }
            else
            {
                OpenAllBigCells();
            }

            base.Turn(symbol, number);
        }
    }

    

    public override bool IsFinal(ETicOrTac symbol)
    {
        int[] indexes = new int[Rows];
        int rows = lin.GetUpperBound(0) + 1;
        int columns = lin.Length / rows;
        for (int i = 0; i < rows; i++)
        {
            bool result = true;
            int indIndexes = 0;
            for (int j = 0; j < columns && result; j++)
            {
                int ind = lin[i, j];
                if (m_mas[(ind - 1) / Rows, (ind - 1) % Columns].value != symbol)
                    result = false;
                indexes[indIndexes] = ind;
                indIndexes++;
            }
            if (result)
            {
                WinLine = new List<int>(indexes);//ShowWin(indexes[k]);
                return true;
            }
        }
        return false;
    }

    public override bool IsFreePlace(int number)
    {
        Mas2d cell = m_mas[number / 9 / m_rows , number / 9 % m_columns];
        if (cell.value != null)
            return false;
        if (cell.IsClose)
            return false;
        return cell.ticTacToe.IsFreePlace(number % 9);
    }

    private void OpenAllBigCells()
    {
        for (int i = 0; i < m_rows; i++)
            for (int j = 0; j < m_columns; j++)
                m_mas[i, j].IsClose = false;
    }


    private void CloseAllBigCells(int indi, int indj)
    {
        for (int i = 0; i < m_rows; i++)
            for (int j = 0; j < m_columns; j++)
            {
                if (indi == i && indj == j)
                {
                    m_mas[i, j].IsClose = false;
                    continue;
                }
                m_mas[i, j].IsClose = true;
            }
    }


    public List<bool> CloseCells
    {
        get
        {
            List<bool> closeCells = new List<bool>();
            //int count = 0;
            foreach (Mas2d element in m_mas)
            {
                //if (element.IsClose)
                //{

                //}

                closeCells.Add(element.IsClose);
                //count++;
            }
            return closeCells;
        }
    }

    public override void ReturnTurn()
    {
        int number = listTurns[listTurns.Count - 1];
        Mas2d cell = m_mas[number / 9 / m_rows, number / 9 % m_columns];
        cell.ticTacToe.ReturnTurn();
        cell.value = null;
        base.ReturnTurn();
        if (listTurns.Count - 1 < 0) {
            OpenAllBigCells();
        }
        number = listTurns[listTurns.Count - 1];
        if (m_mas[(number % 9) / Rows, (number % 9) % Columns].value == null)
        {
            CloseAllBigCells(number % 9 / Rows, number % 9 % Columns);
        }
        else
        {
            OpenAllBigCells();
        }
        
    }

    

    public override int BestVariant(ETicOrTac symbol)
    {
        Dictionary<int, double> bigCellsBadrating = new Dictionary<int, double>();
        Dictionary < int,Dictionary<int, double>> goodCellsVariants = new Dictionary<int, Dictionary<int, double>>();

        //we find bad turns
        int count = 0;
        foreach (Mas2d variant in m_mas)
        {
            //int result = 0;
            if (variant.value != null)
            {
                bigCellsBadrating.Add(count, 10);
            }
            else
            {

                bigCellsBadrating.Add(count ,variant.ticTacToe.BestVariantWithRating(symbol.InverseETicOrTac()).rating);
            }
            count++;
        }


        count = 0;
        foreach (Mas2d variant in m_mas)
        {
            if (!variant.IsClose)
            {
                Dictionary<int, double> goodVariants = variant.ticTacToe.Variants(symbol);
                if (goodVariants.Count > 0)
                {
                    foreach (int key in bigCellsBadrating.Keys)
                    {
                        double value;
                        if (goodVariants.TryGetValue(key, out value))
                        {
                            goodVariants[key] -= bigCellsBadrating[key];
                        }
                    }
                    goodCellsVariants.Add(count, goodVariants);
                }
            }
            count++;
        }
        //we find max sum from all big cells
        double max = goodCellsVariants.Max(x => x.Value.Max(y => y.Value));

        KeyValuePair<int, Dictionary<int, double>> bigElement = goodCellsVariants.Where(x => x.Value.Max(y => y.Value) >= max).OrderBy(u => u.Value.Max(z => z.Value)).First();//.Select(element => element.Key).ToArray();
        int bigInd = bigElement.Key;
        //if (bigCells.Contains(this.MaxCells / 2))
        //{
        //    ind = this.MaxCells / 2;
        //}
        //else
        //{
        //    ind = bigCells.First();
        //}

        double littleMax = bigElement.Value.Values.Max();
        int[] elements = bigElement.Value./*Concat(m_mas[bigInd / m_rows, bigInd % m_columns].ticTacToe.Variants(symbol)).*/Where(x => x.Value >= littleMax).OrderBy(x => x.Value).Select(x => x.Key).ToArray();
        int ind =  0;
        //if (elements.Contains(9 / 2))
        //{
        //    ind = 9 / 2;
        //}
        //else
        //{
            ind = elements.First();
        //}
        return bigInd * 9 + ind;
    }

    public override Dictionary<int, double> Variants(ETicOrTac symbol)
    {
        throw new System.NotImplementedException();
    }
}
