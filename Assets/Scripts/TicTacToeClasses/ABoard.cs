﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ABoard  : ICloneable
{
    public delegate void WinDelegate(ETicOrTac ticOrTac);
    public event WinDelegate WinEvent;
    public delegate void WinDelegateWithWinLine(ETicOrTac ticOrTac, List<int> winLine);
    public event WinDelegateWithWinLine WinEventWithWinLine;

    private List<int> m_winLine;
    protected List<int> listTurns;
    private bool gameOver = false;
    private AGamer m_firstGamer;
    private AGamer m_secondGamer;
    private AGamer m_mainGamer;

    protected List<int> WinLine
    {
        set { m_winLine = value; }
    }

    public abstract int MinCells { get; }
    public abstract int MaxCells { get;  }
    public abstract int FreePlacesCount { get; }
    public abstract bool IsFool { get; }

    public AGamer MainGamer { get => m_mainGamer; private set => m_mainGamer = value; }

    public ABoard(AGamer firstGamer, AGamer secondGamer)
    {
        listTurns = new List<int>();
        m_firstGamer = firstGamer;
        firstGamer.EndTurn += ChangeGamer;
        secondGamer.EndTurn += ChangeGamer;
        m_secondGamer = secondGamer;
        MainGamer = m_firstGamer;
        firstGamer.Initialize(this,ETicOrTac.X);
        secondGamer.Initialize(this, ETicOrTac.O);
        MainGamer.NewTurn();
    }

    public ABoard() { listTurns = new List<int>(); }



    protected void NotifyAboutWin(ETicOrTac ticOrTac)
    {
        WinEvent?.Invoke(ticOrTac);
        //if(m_winLine != null)
            WinEventWithWinLine?.Invoke(ticOrTac, m_winLine);
    }



    public abstract bool IsFinal(ETicOrTac symbol);

    //protected abstract bool IsFool();

    public virtual void Turn(ETicOrTac symbol, int number)
    {
        listTurns.Add(number);
        Turn(symbol);
    }

    public virtual void Turn(ETicOrTac symbol)
    {
        if (IsFinal(symbol))
        {
            NotifyAboutWin(symbol);
            gameOver = true;
            return;
        }

        if (IsFool)
        {
            NotifyAboutWin(ETicOrTac.Draw);
            gameOver = true;
            return;
        }
    }

    private void ChangeGamer()
    {
        if(MainGamer == m_firstGamer)
        {
            MainGamer = m_secondGamer;
        }
        else
        {
            MainGamer = m_firstGamer;
        }

        Debug.Log(MainGamer.Symbol.ToString());
        if(!gameOver)
            MainGamer.NewTurn();
    }
    
    private void SwapGamer(ref AGamer firstGamer,ref AGamer secondGamer)
    {
        AGamer temp = firstGamer;
        firstGamer = secondGamer;
        secondGamer = temp;
    }

    public abstract void Clear();

    public abstract bool IsFreePlace(int number);

    public abstract object Clone();

    public abstract int BestVariant(ETicOrTac symbol);
    public abstract Dictionary<int, double> Variants(ETicOrTac symbol);

    public virtual void ReturnTurn()
    {
        listTurns.Remove(listTurns[listTurns.Count - 1]);
    }
}
