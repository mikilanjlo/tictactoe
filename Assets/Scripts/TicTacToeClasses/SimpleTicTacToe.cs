﻿using Assets.Scripts;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTicTacToe 
{

    struct Turns
    {
        public int cell;
        public ETicOrTac XO;
        public Turns(int cell, ETicOrTac XO)
        {
            this.cell = cell;
            this.XO = XO;
        }
    }
    #region Fields
    private static int m_rows = 3;
    private ETicOrTac?[,] m_mas = new ETicOrTac?[m_rows, 3];
    private List<int> winLine;
    public ETicOrTac XO { get; private set; }
    private static readonly int[,] lin = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 1, 4, 7 }, { 2, 5, 8 }, { 3, 6, 9 }, { 1, 5, 9 }, { 3, 5, 7 } };
    private int levelPercent = 100 - (int)(Random.value * 25);
    private List<Turns> m_turns;
    #endregion

    #region Constructor
    public SimpleTicTacToe()
    {
        m_turns = new List<Turns>();
        CreateNewGame();
    }
    #endregion

    #region Public Methods
    #region Procedure
    public virtual void CreateNewGame()
    {
        winLine = new List<int>();
        MasInNull();
        XO = ETicOrTac.X; //GetRanomXorO();
    }

    public void MasInNull()
    {
        int columns = m_mas.Length / m_rows;

        for (int i = 0; i < m_rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                //m_mas[i, j] = null;
            }
        }
    }

    public void TakeThePlace(int number)
    {
        m_mas[(number - 1) / m_rows, (number - 1) % m_rows] = XO;
        m_turns.Add(new Turns(number, XO));
        if (XO == ETicOrTac.X)
            XO = ETicOrTac.O;
        else
            XO = ETicOrTac.X;
    }

    public void ReturnTurn()
    {
        if (m_turns.Count > 0)
        {
            //m_mas[(m_turns[m_turns.Count - 1].cell - 1) / m_rows, (m_turns[m_turns.Count - 1].cell - 1) % m_rows] = null;
            m_turns.RemoveAt(m_turns.Count - 1);
        }
    }

    public void ChangeXO()
    {
        if (XO == ETicOrTac.X)
            XO = ETicOrTac.O;
        else
            XO = ETicOrTac.X;
    }
    #endregion

    #region Return ETicOrTac
    public ETicOrTac GetRanomXorO()
    {
        float number = Random.value;
        if (number > 0.5)
            return ETicOrTac.X;
        else
            return ETicOrTac.O;
    }

    

    public ETicOrTac? WhoWin()
    {
        if (Final(ETicOrTac.X))
        {
            return ETicOrTac.X;
        }
        else
        {
            if (Final(ETicOrTac.O))
            {
                return ETicOrTac.O;
            }
            else
            {
                if (IsFull(m_mas))
                {
                    return ETicOrTac.Draw;
                }
            }
        }
        return null;
    }
    #endregion


    public  bool IsFreePlace(int ind)
    {
        return true;
        //return m_mas[(ind - 1) / m_rows, (ind - 1) % m_rows] == null;
    }

    
    public int ComputerTurn()
    {
        ETicOrTac?[,] newMas = new ETicOrTac?[m_rows, 3];
        for (int i = 0; i < m_rows; i++)
        {
            for (int j = 0; j < m_mas.Length / m_rows; j++)
            {
                newMas[i, j] = m_mas[i, j];
            }
        }
        Dictionary<int, int> ratings = new Dictionary<int, int>();
        for (int i = 0; i < m_rows; i++)
        {
            //for (int j = 0; j < m_mas.Length / m_rows; j++)
            //{
            //    if (newMas[i, j] == null)
            //    {
            //        newMas[i, j] = ETicOrTac.O;
            //        ratings.Add(i * m_rows + j, SimulateTurn(newMas, ETicOrTac.X));
            //        newMas[i, j] = null;
            //    }
            //}
        }
        int max = -200000000;
        int ind = 0;
        foreach (KeyValuePair<int, int> keyValue in ratings)
            if (max < keyValue.Value)
            {
                max = keyValue.Value;
                ind = keyValue.Key;
            }
        double num = Random.value;
        double num2 = (levelPercent / 100);
        Debug.Log(num);
        return (num < (levelPercent / 100f) ? ind : RandomFreePlace()) + 1;
    }


    public int[] GetWinLine()
    {
        return winLine.ToArray();
    }

    public ETicOrTac? GetSymbolXOCell(int ind)
    {
        return m_mas[(ind - 1) / m_rows, (ind - 1) % m_rows];
    }

    public static bool IsFinal(ETicOrTac symbol, ETicOrTac?[,] newMas)
    {
        int rows = lin.GetUpperBound(0) + 1;
        int columns = lin.Length / rows;
        for (int i = 0; i < rows; i++)
        {
            bool result = true;
            for (int j = 0; j < columns && result; j++)
            {
                int ind = lin[i, j];
                if (newMas[(ind - 1) / m_rows, (ind - 1) % m_rows] != symbol)
                    result = false;
            }
            if (result)
            {
                return true;
            }
        }
        return false;
    }
    #endregion

    #region Private Methods
    private bool Final(ETicOrTac symbol)
    {
        int rows = lin.GetUpperBound(0) + 1;
        int columns = lin.Length / rows;
        int[] indexes = new int[m_rows];

        for (int i = 0; i < rows; i++)
        {
            bool result = true;
            int indIndexes = 0;
            for (int j = 0; j < columns && result; j++)
            {
                int ind = lin[i, j];
                if (m_mas[(ind - 1) / m_rows, (ind - 1) % m_rows] != symbol)
                    result = false;
                    indexes[indIndexes] = ind;
                    indIndexes++;
            }
            if (result)
            {
                    for (int k = 0; k < indexes.Length; k++)
                        winLine.Add(indexes[k]);//ShowWin(indexes[k]);
                return true;
            }
        }
        return false;
    }

    private int? WinComputerResults(ETicOrTac?[,] newMas)
    {
        if (IsFinal(ETicOrTac.X, newMas))
        {
            return -1;
        }
        else
        {
            if (IsFinal(ETicOrTac.O, newMas))
            {
                return 1;
            }
            else if (IsFull(newMas)) { return 0; }
        }
        return null;
    }


    public static bool IsFull(ETicOrTac?[,] mas)
    {
        //foreach (ETicOrTac e in mas)
        //    if (e == null)
        //        return false;
        return true;
    }

    private int SimulateTurn(ETicOrTac?[,] newMas, ETicOrTac symbol)
    {
        int result = 0;
        if (WinComputerResults(newMas) == null)
        {
            for (int i = 0; i < m_rows; i++)
            {
                for (int j = 0; j < m_mas.Length / m_rows; j++)
                {
                    //if (newMas[i, j] == null)
                    //{
                    //    newMas[i, j] = symbol;
                    //    result += SimulateTurn(newMas, symbol == ETicOrTac.O ? ETicOrTac.X : ETicOrTac.O) * ( GetCountFreePlaces(newMas) + 1);
                    //    newMas[i, j] = null;
                    //}
                }
            }
            return result;
        }
        else
            return result + (int)WinComputerResults(newMas);
    }

    private int RandomFreePlace()
    {
        List<int> masIndexes = new List<int>();
        int columns = m_mas.Length / m_rows;
        for (int i = 0; i < m_rows; i++)
        {
            for (int j = 0; j < m_mas.Length / m_rows; j++)
            {
                //if (m_mas[i, j] == null)
                //{
                //    masIndexes.Add(i * columns + j);
                //}
            }
        }
        return masIndexes[(int)(Random.value * masIndexes.Count)];
    }

    public int GetCountFreePlaces()
    {
        return GetCountFreePlaces(m_mas);
    }

    private int GetCountFreePlaces(ETicOrTac?[,] mas)
    {
        int count = 0;
        for (int i = 0; i < m_rows; i++)
        {
            for (int j = 0; j < mas.Length / m_rows; j++)
            {
                //if (mas[i, j] == ETicOrTac.Nul)
                //{
                //    count++;
                //}
            }
        }
        return count;
    }
    #endregion
}
