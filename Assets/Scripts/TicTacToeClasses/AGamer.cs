﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AGamer 
{
    public delegate void ChangeTurn();
    public event ChangeTurn StartTurn;
    public event ChangeTurn EndTurn;
    public delegate void MakeMove(int cell);
    public event MakeMove MakeTurn;


    protected ABoard board;
    private ETicOrTac m_symbol;

    public ETicOrTac Symbol { get => m_symbol; private set => m_symbol = value; }

    public void Initialize(ABoard board, ETicOrTac ticOrTac)
    {
        this.board = board;
        Symbol = ticOrTac;
    }

    public virtual void NewTurn()
    {
        StartTurn?.Invoke();
        //Turn();
    }

    public virtual void Turn(int cell)
    {
        MakeTurn?.Invoke(cell);
        EndTurn?.Invoke();
    }

    public virtual void Turn()
    {
        EndTurn?.Invoke();
    }

}
