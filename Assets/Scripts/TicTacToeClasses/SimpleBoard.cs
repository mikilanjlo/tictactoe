﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.TicTacToeClasses
{
    /// <summary>
    /// Board = mas[3,3]
    /// </summary>
    public class SimpleBoard : ABoard
    {
        private static int[,] lin = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 1, 4, 7 }, { 2, 5, 8 }, { 3, 6, 9 }, { 1, 5, 9 }, { 3, 5, 7 } };
        private static int m_rows = 3;
        private static int m_columns = 3;
        private ETicOrTac?[,] m_mas = new ETicOrTac?[Rows, Columns];

        public static int Rows { get => m_rows; }
        public static int Columns { get => m_columns; }
        public override int MaxCells { get => m_rows * m_columns; }
        public override int MinCells => 0;

        public override bool IsFool
        {
            get
            {
                foreach (ETicOrTac? e in m_mas)
                    if (e == null)
                        return false;
                return true;
            }
         }

        public override int FreePlacesCount
        {
            get
            {
                int count = 0;
                foreach (ETicOrTac? symbol in m_mas)
                    if (symbol == null)
                        count++;
                return count;
            }
        }

        public static int[,] Lin { get => lin;  }

        public SimpleBoard(AGamer firstGamer, AGamer secondGamer) : base(firstGamer, secondGamer) { }
        public SimpleBoard(ETicOrTac?[,] array) : base()
        {
            m_mas = array;
        }
        public SimpleBoard() : base() { }

        public override void Clear()
        {
            m_mas = new ETicOrTac?[Rows, Columns];
        }

        /// <summary>
        /// Function Of turn on board which has  mas[3,3]
        /// </summary>
        /// <param name="ticOrTac"></param>
        /// <param name="number">index of mas[3,3]. number less than 9 and more than -1. 
        /// i = number / 3, j = number % 3</param>
        public override void Turn(ETicOrTac ticOrTac, int number)
        {
            if(IsTurn(ticOrTac, number / Rows, number % Columns))
                base.Turn(ticOrTac, number);
        }

        public bool IsTurn(ETicOrTac ticOrTac, int i, int j)
        {
            if (IsFreePlace(i, j))
            {
                m_mas[i, j] = ticOrTac;
                return true;
                //base.Turn(ticOrTac);
            }
            return false;
        }

        public override bool IsFinal(ETicOrTac symbol)
        {
            bool result = IsFinal(symbol, m_mas,out List<int> winLine);
            //if(result)
            WinLine = winLine;
            return result;
        }

        public static bool IsFinal(ETicOrTac symbol,ETicOrTac?[,] array, out List<int> winLine)
        {
            int[] indexes = new int[Rows];
            winLine = null;
            int rows = Lin.GetUpperBound(0) + 1;
            int columns = Lin.Length / rows;
            for (int i = 0; i < rows; i++)
            {
                bool result = true;
                int indIndexes = 0;
                for (int j = 0; j < columns ; j++)
                {
                    int ind = Lin[i, j];
                    if (array[(ind - 1) / Rows, (ind - 1) % Columns] != symbol)
                        result = false;
                    indexes[indIndexes] = ind;
                    indIndexes++;
                }
                if (result)
                {
                    winLine = new List<int>(indexes);//ShowWin(indexes[k]);
                    return true;
                }
            }
            return false;
        }

        //protected override bool IsFool()
        //{
        //    foreach (ETicOrTac? e in m_mas)
        //        if (e == null)
        //            return false;
        //    return true;
        //}

        public override void ReturnTurn()
        {
            int number = listTurns[listTurns.Count - 1];
            m_mas[number / Rows, number % Columns] = null;
            base.ReturnTurn();
        }

        public bool IsFreePlace(int i, int j)
        {
            return m_mas[i,j] == null;
        }

        public override bool IsFreePlace(int number)
        {
            return IsFreePlace(number / Rows, number % Columns);
        }

        public override object Clone()
        {           
            return new SimpleBoard(m_mas.Copy());
        }

        public override int BestVariant(ETicOrTac symbol)
        {
            return BestVariantWithRating(symbol).index;
        }

        public  (int index, double rating) BestVariantWithRating(ETicOrTac symbol)
        {
            Dictionary<int, double> ratings = Variants(symbol);
            double max = ratings.Values.Max();
            int[] elements = ratings.Where(x => x.Value >= max).Select(x => x.Key).ToArray();
            int ind = 0;
            if (elements.Contains(this.MaxCells / 2))
            {
                ind = this.MaxCells / 2;
            }
            else
            {
                ind = elements.First();
            }

            return (ind,ratings[ind]);
        }

        public override Dictionary<int,double> Variants(ETicOrTac symbol)
        {
            Dictionary<int, double> ratings = new Dictionary<int, double>();
            int rows = Lin.GetUpperBound(0) + 1;
            int columns = Lin.Length / rows;
            for (int k = this.MinCells; k < this.MaxCells; k++)
            {
                double result = 0;
                if (this.IsFreePlace(k))
                {
                    for (int i = 0; i < rows; i++)
                    {
                        bool isIncludes = false;
                        for (int j = 0; j < columns; j++)
                            if (Lin[i, j] == k + 1)
                            {
                                isIncludes = true;
                                break;
                            }
                        if (isIncludes)
                        {
                            ETicOrTac?[] array = { m_mas[(Lin[i, 0] - 1) / Rows, (Lin[i, 0] - 1) % Columns], m_mas[(Lin[i, 1] - 1) / Rows, (Lin[i, 1] - 1) % Columns], m_mas[(Lin[i, 2] - 1) / Rows, (Lin[i, 2] - 1) % Columns] };
                            for (int j = 0; j < columns; j++)
                                if (Lin[i, j] == k + 1)
                                {
                                    array[j] = symbol;
                                    break;
                                }

                            ETicOrTac?[] notBadVariant = { symbol.InverseETicOrTac(), symbol.InverseETicOrTac(), symbol };
                            ETicOrTac?[] goodVariant = { symbol, symbol, symbol };
                            ETicOrTac?[] array2 = { symbol, symbol, null };
                            Array.Sort(array);
                            Array.Sort(notBadVariant);
                            Array.Sort(goodVariant);
                            if (array.SequenceEqual(notBadVariant))
                                result += 5;
                            if (array.SequenceEqual(goodVariant))
                                result += 10;
                            if (array.SequenceEqual(array2))
                                result += 2;
                        }
                    }
                    ratings.Add(k, result);
                }

            }
            return ratings;
        }
    }
}
