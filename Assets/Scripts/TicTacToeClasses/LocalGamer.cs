﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalGamer : AGamer
{

    public override void Turn( int cell)
    {
        if (board.IsFreePlace(cell))
        {
            board.Turn(Symbol, cell);
            base.Turn();
        }
    }
}
