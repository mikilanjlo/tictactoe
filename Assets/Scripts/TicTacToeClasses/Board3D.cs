﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Assets.Scripts.TicTacToeClasses;
using UnityEngine;

public class Board3D : ABoard
{
    
    private ETicOrTac?[,,] m_mas = new ETicOrTac?[3,3,3];
    public override int MinCells => 0;

    public override int MaxCells => 3*3*3;

    public override int FreePlacesCount
    {
        get
        {
            int count = 0;
            foreach (ETicOrTac? symbol in m_mas)
                if (symbol == null)
                    count++;
            return count;
        }
    }

    public override bool IsFool
    {
        get
        {
            foreach (ETicOrTac? e in m_mas)
                if (e == null)
                    return false;
            return true;
        }
    }

    public Board3D(AGamer firstGamer, AGamer secondGamer):base(firstGamer,secondGamer)
    {

    }
    public Board3D(ETicOrTac?[,,] array) : base()
    {
        m_mas = array;
    }
    public Board3D() : base() { }

    public override void Turn(ETicOrTac symbol, int number)
    {
        if (IsFreePlace(number))
        {
            int i = number / 9;
            int j = number % 9 / 3;
            int k = number % 3;
            m_mas[i, j, k] = symbol;
            base.Turn(symbol, number);
        }
        
    }

    public override int BestVariant(ETicOrTac symbol)
    {
        Dictionary<int, double> ratings = new Dictionary<int, double>();
        for (int number = MinCells; number < MaxCells; number++)
        {
            if (number != 9 + 3 + 1)
            {
                if (IsFreePlace(number))
                {
                    double rating = 0;
                    int indi = number / 9;
                    int indj = number % 9 / 3;
                    int indk = number % 3;
                    ETicOrTac?[,] tempArray = new ETicOrTac?[m_mas.GetLength(1), m_mas.GetLength(2)];
                    for (int j = 0; j < m_mas.GetLength(1); j++)
                        for (int k = 0; k < m_mas.GetLength(2); k++)
                                tempArray[j, k] = m_mas[indi, j, k];
                    rating += GetRating(tempArray, indj, indk, symbol);

                    tempArray = new ETicOrTac?[m_mas.GetLength(1), m_mas.GetLength(0)];
                    for (int j = 0; j < m_mas.GetLength(1); j++)
                        for (int i = 0; i < m_mas.GetLength(0); i++)
                                tempArray[j, i] = m_mas[i, j, indk];
                    rating += GetRating(tempArray, indj, indi, symbol);

                    tempArray = new ETicOrTac?[m_mas.GetLength(0), m_mas.GetLength(2)];
                    for (int i = 0; i < m_mas.GetLength(0); i++)
                        for (int k = 0; k < m_mas.GetLength(2); k++)
                                tempArray[i, k] = m_mas[i, indj, k];
                    rating += GetRating(tempArray, indi, indk,symbol);
                    ratings.Add(number, rating);
                }
            }
        }
        double max = ratings.Values.Max();
        int[] elements = ratings.Where(x => x.Value >= max).Select(x => x.Key).ToArray();
        int ind = 0;
        ind = elements.First();
        return ind;
    }

    private double GetRating(ETicOrTac?[,] array, int indi, int indj, ETicOrTac symbol)
    {
        int rows = SimpleBoard.Lin.GetUpperBound(0) + 1;
        int columns = SimpleBoard.Lin.Length / rows;
        int number = indi * 3 + indj;
        double result = 0;
        for (int i = 0; i < rows; i++)
        {
            bool isIncludes = false;
            for (int j = 0; j < columns; j++)
                if (SimpleBoard.Lin[i, j] == number + 1)
                {
                    isIncludes = true;
                    break;
                }
            if (isIncludes)
            {
                ETicOrTac?[] new_array = { array[(SimpleBoard.Lin[i, 0] - 1) / 3, (SimpleBoard.Lin[i, 0] - 1) % 3], array[(SimpleBoard.Lin[i, 1] - 1) / 3, (SimpleBoard.Lin[i, 1] - 1) % 3], array[(SimpleBoard.Lin[i, 2] - 1) / 3, (SimpleBoard.Lin[i, 2] - 1) % 3] };
                for (int j = 0; j < columns; j++)
                    if (SimpleBoard.Lin[i, j] == number + 1)
                    {
                        new_array[j] = symbol;
                        break;
                    }

                ETicOrTac?[] notBadVariant = { symbol.InverseETicOrTac(), symbol.InverseETicOrTac(), symbol };
                ETicOrTac?[] goodVariant = { symbol, symbol, symbol };
                ETicOrTac?[] array2 = { symbol, symbol, null };
                Array.Sort(new_array);
                Array.Sort(notBadVariant);
                Array.Sort(goodVariant);
                if (new_array.SequenceEqual(notBadVariant))
                    result += 5;
                if (new_array.SequenceEqual(goodVariant))
                    result += 10;
                if (new_array.SequenceEqual(array2))
                    result += 2;
            }
        }
        return result;
    }

    public override void Clear()
    {
        m_mas = new ETicOrTac?[3, 3, 3];
    }

    public override object Clone()
    {
        return new Board3D(m_mas.Copy());
    }

    public override bool IsFinal(ETicOrTac symbol)
    {
        for (int i = 0; i < m_mas.GetLength(0); i++)
        {
            ETicOrTac?[,] tempArray = new ETicOrTac?[m_mas.GetLength(1), m_mas.GetLength(2)];
            bool final = false;
            for (int j = 0; j < m_mas.GetLength(1); j++)
                for (int k = 0; k < m_mas.GetLength(2); k++)
                {
                    if (i == 1 && j == 1 && k == 1)
                        break;
                    tempArray[j, k] = m_mas[i, j, k];
                }
            final = SimpleBoard.IsFinal(symbol, tempArray,out List<int> winLine);
            if (final)
                return true;
        }
        for (int k = 0; k < m_mas.GetLength(2); k++)
        {
            ETicOrTac?[,] tempArray = new ETicOrTac?[m_mas.GetLength(1), m_mas.GetLength(0)];
            bool final = false;
            for (int j = 0; j < m_mas.GetLength(1); j++)
                for (int i = 0; i < m_mas.GetLength(0); i++)
                {
                    if (i == 1 && j == 1 && k == 1)
                        break;
                    tempArray[j, i] = m_mas[i, j, k];
                }
            final = SimpleBoard.IsFinal(symbol, tempArray, out List<int> winLine);
            if (final)
                return true;
        }
        for (int j = 0; j < m_mas.GetLength(1); j++)
        {
            ETicOrTac?[,] tempArray = new ETicOrTac?[m_mas.GetLength(0), m_mas.GetLength(2)];
            bool final = false;
            for (int i = 0; i < m_mas.GetLength(0); i++)
                for (int k = 0; k < m_mas.GetLength(2); k++)
                {
                    if (i == 1 && j == 1 && k == 1)
                        break;
                    tempArray[i, k] = m_mas[i, j, k];
                }
            final = SimpleBoard.IsFinal(symbol, tempArray, out List<int> winLine);
            if (final)
                return true;
        }
        
        return false;
    }

    

    public override bool IsFreePlace(int number)
    {
        int i = number / 9;
        int j = number % 9 / 3;
        int k = number % 3;
        return m_mas[i, j, k] == null;
    }

    public override Dictionary<int, double> Variants(ETicOrTac symbol)
    {
        throw new System.NotImplementedException();
    }
}
