﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicTacToe2d 
{
    struct Mas2d
    {
        public ETicOrTac? value;
        public SimpleTicTacToe ticTacToe;
        
        public Mas2d(SimpleTicTacToe ticTacToe)
        {
            this.ticTacToe = ticTacToe;
            value = null;
        }

        public Mas2d(ETicOrTac? XO,SimpleTicTacToe ticTacToe)
        {
            this.ticTacToe = ticTacToe;
            value = XO;
        }

        public static ETicOrTac?[,] ToTicTacToeArray(Mas2d[] array)
        {
            ETicOrTac?[,] resultMas = new ETicOrTac?[3,3];
            int ind = 0;
            for(int i = 0; i < 3; i++)
                for(int j = 0; j < 3; j++)
                {
                    resultMas[i, j] = array[ind].value;
                    ind++;
                }
            return resultMas;
        }
    }
    struct Rating
    {
        public int rating;
        public int bigCell;
        public int cell;
        public Rating(int bigCell,int cell,int rating)
        {
            this.bigCell = bigCell;
            this.cell = cell;
            this.rating = rating;
        }
        public static bool operator >(Rating a, Rating b)
        {
            return a.rating > b.rating;
        }
        public static bool operator <(Rating a, Rating b)
        {
            return a.rating < b.rating;
        }

    }

    private int id = 0;
    private Mas2d[] m_mas;
    public SimpleTicTacToe m_ticTacToe;
    private int? m_freeBigCell = null;
    public ETicOrTac XO { get { return m_ticTacToe.XO; } }
    public int? bigTurn {get; private set;}


    public TicTacToe2d()
    {
        m_ticTacToe = new SimpleTicTacToe();
        CreateNewGame();
    }

    public  void CreateNewGame()
    {

        Mas2d[] mas = new Mas2d[9];
        for (int i = 0; i < 9; i++)
            mas[i] = new Mas2d(new SimpleTicTacToe());
        m_mas = mas;

        m_ticTacToe.CreateNewGame();
        //base.CreateNewGame();
    }

    public bool IsFreePlace(int bigCell, int ind)
    {
        return m_mas[bigCell - 1].ticTacToe.IsFreePlace(ind);
    }

    public void TakeThePlace(int bigCell, int number)
    {
        if (m_mas[bigCell - 1].ticTacToe.XO != m_ticTacToe.XO)
            m_mas[bigCell - 1].ticTacToe.ChangeXO();
        m_mas[bigCell - 1].ticTacToe.TakeThePlace(number);
        
        if(!MakeBigTurn(bigCell))
            m_ticTacToe.ChangeXO();
    }

    public ETicOrTac? GetSymbolXOBigCell(int bigCell)
    {
        return m_mas[bigCell - 1].value; //m_ticTacToe.GetSymbolXOCell(bigCell);
    }

    public int[] GetWinLine()
    {
        return m_ticTacToe.GetWinLine();
    }

    public void SetFreeBigCell(int? bigCell)
    {
        m_freeBigCell = bigCell;
    }

    private bool MakeBigTurn(int bigCell)
    {
        int i = bigCell - 1;
        //for (int i = 0; i < m_mas.Length; i++)
        //{
            if (m_mas[i].value == null)
            {
                ETicOrTac? whoWin = m_mas[i].ticTacToe.WhoWin();
                
                switch (whoWin)
                {
                    case ETicOrTac.X:
                        m_mas[i].value = ETicOrTac.X;
                        bigTurn = i + 1;
                    m_ticTacToe.TakeThePlace(bigCell);
                    return true;
                        
                    case ETicOrTac.O:
                        m_mas[i].value = ETicOrTac.O;
                        bigTurn = i + 1;
                    m_ticTacToe.TakeThePlace(bigCell);
                    return true;
                case ETicOrTac.Draw:
                        m_mas[i].value = ETicOrTac.Draw;
                        bigTurn = i + 1;
                    m_ticTacToe.TakeThePlace(bigCell);
                    return true;

            }
            }
        return false;
        //}
    }

    private void SimulateBigTurn(int bigCell,Mas2d[] newMas)
    {
        int i = bigCell - 1;
        if (newMas[i].value == null)
        {
            ETicOrTac? whoWin = newMas[i].ticTacToe.WhoWin();
            switch (whoWin)
            {
                case ETicOrTac.X:
                    newMas[i].value = ETicOrTac.X;
                    break;
                case ETicOrTac.O:
                    newMas[i].value = ETicOrTac.O;
                    break;
                case ETicOrTac.Draw:
                    newMas[i].value = ETicOrTac.Draw;
                    break;
            }
        }
    }

    public ETicOrTac? WhoWin()
    {
        return m_ticTacToe.WhoWin();
    }

    public (int,int) ComputerTurn()
    {
        id = 0;
        Mas2d[] newMas = new Mas2d[m_mas.Length];
        for (int i = 0; i < m_mas.Length; i++)
        {
            newMas[i] = new Mas2d(m_mas[i].value, m_mas[i].ticTacToe);
        }
        
        List<Rating> ratings = new List<Rating>();
        for (int i = 0; i < newMas.Length; i++)
        {
            if (m_freeBigCell != null)
                if (i != m_freeBigCell - 1)
                    continue;
            if (newMas[i].value != null)
                continue;
            for (int j = 1; j <= 9; j++)
            {
                int? newFreePlace;
                if (GetSymbolXOBigCell(j) == null)
                    newFreePlace = j;
                else
                    newFreePlace = null;
                if (newMas[i].ticTacToe.GetSymbolXOCell(j) == null)
                {
                    if (newMas[i].ticTacToe.XO != ETicOrTac.O)
                        newMas[i].ticTacToe.ChangeXO();

                    newMas[i].ticTacToe.TakeThePlace(j);
                    ratings.Add(new Rating(i+1,j,SimulateTurn(newMas, newFreePlace , ETicOrTac.X, i+1)));
                    newMas[i].ticTacToe.ReturnTurn();
                }
            }
            newMas[i].value = null;
        }
        Rating max = ratings[0];
        foreach (Rating value in ratings)
            if (max < value)
            {
                max = value;
            }
        return (max.bigCell,max.cell);
    }

    private  int SimulateTurn(Mas2d[] newMas, int? freePlace, ETicOrTac? symbol,int bigCell)
    {
        id++;
        SimulateBigTurn(bigCell,newMas);
        int result = 0;
        if (WinComputerResults(Mas2d.ToTicTacToeArray(newMas)) == null)
        {
            for (int i = 0; i < newMas.Length; i++)
            {
                if (freePlace != null)
                    if (i != freePlace - 1)
                        continue;
                if (newMas[i].value != null)
                    continue;
                
                for (int j = 1; j <= 9; j++)
                {
                    int? newFreePlace;
                    if (GetSymbolXOBigCell(j) == null)
                        newFreePlace = j;
                    else
                        newFreePlace = null;
                    if (newMas[i].ticTacToe.GetSymbolXOCell(j) == null)
                    {
                        if (newMas[i].ticTacToe.XO != symbol)
                            newMas[i].ticTacToe.ChangeXO();
                        newMas[i].ticTacToe.TakeThePlace(j);
                        result += SimulateTurn(newMas, newFreePlace ,symbol == ETicOrTac.O ? ETicOrTac.X : ETicOrTac.O, i+1) * GetCountFreePlaces(newMas);
                        newMas[i].ticTacToe.ReturnTurn();
                    }
                }
                newMas[i].value = null;
            }
            return result;
        }
        else
            return result + (int)WinComputerResults(Mas2d.ToTicTacToeArray(newMas));        
    }

    private int? WinComputerResults(ETicOrTac?[,] newMas)
    {
        if (SimpleTicTacToe.IsFinal(ETicOrTac.X, newMas))
        {
            return -1;
        }
        else
        {
            if (SimpleTicTacToe.IsFinal(ETicOrTac.O, newMas))
            {
                return 1;
            }
            else if (SimpleTicTacToe.IsFull(newMas)) { return 0; }
        }
        return null;
    }

    private int GetCountFreePlaces(Mas2d[] mas)
    {
        int sum = 0;
        for (int i = 0; i < mas.Length; i++)
            sum += mas[i].ticTacToe.GetCountFreePlaces();
        return sum;
    }
}
