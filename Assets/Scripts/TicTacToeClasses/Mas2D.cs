﻿using Assets.Scripts;
using Assets.Scripts.TicTacToeClasses;
using System.Collections;
public class Mas2d
{
    public ETicOrTac? value;
    public SimpleBoard ticTacToe;
    public bool IsClose;

    public Mas2d(SimpleBoard ticTacToe)
    {
        this.ticTacToe = ticTacToe;
        value = null;
        IsClose = false;
    }

    public Mas2d(ETicOrTac? XO, SimpleBoard ticTacToe)
    {
        this.ticTacToe = ticTacToe;
        value = XO;
        IsClose = false;
    }

    public Mas2d(ETicOrTac? XO, SimpleBoard ticTacToe, bool close)
    {
        this.ticTacToe = ticTacToe;
        value = XO;
        IsClose = close;
    }

    public static ETicOrTac?[,] ToTicTacToeArray(Mas2d[] array)
    {
        ETicOrTac?[,] resultMas = new ETicOrTac?[3, 3];
        int ind = 0;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
            {
                resultMas[i, j] = array[ind].value;
                ind++;
            }
        return resultMas;
    }

    public static Mas2d[,] GetNewDoubleArray(int rows, int columns)
    {
        Mas2d[,] array = new Mas2d[rows, columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
                array[i, j] = new Mas2d(new SimpleBoard());
        return array;
    }
}
