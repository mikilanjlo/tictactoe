﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gameplay3d : AGameplay
{
    [SerializeField] private GameObject cellPrafab;
    //[SerializeField] private GameObject blockingCellPrafab;
    [SerializeField] private GameObject cellParent;
    [SerializeField] private GameObject cellObjectRotate;
    [SerializeField] private float distanceBetweenCells;
    [SerializeField] private float changeSpaceSpeed;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float differentTime;
    [SerializeField] private float minDistanceBetweenTouch;
    private GameObject[,,] m_cells;
    private Board3D m_board;
    private Vector3 fp;
    private float startTime;
    Vector3 startRotation;
    private bool canMakeFirstClick = false;
    private bool firstclick = false;

    public override void ButtonClick(int bigCell, int cell)
    {
        if (!m_computerTurn && m_GameRun)
        {
            int ind = cell;
            if (m_board.IsFreePlace(ind))
            {
                MakeMoveLocal(ind);
            }
        }
    }

    public override void SetComputerMode(bool on)
    {
        canMakeFirstClick = true;
        base.SetComputerMode(on);
        NewGame();
    }

    public void ObjectClick(string objectName)
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    if (i == 1 && j == 1 && k == 1)
                        continue;
                    if (m_cells[i,j,k].name == objectName)
                    {
                        
                        Debug.Log("vhod2 " + objectName);
                        ButtonClick(0 , i*9 + j * 3 + k);
                        break;
                    }

                }
            }
        }
    }

    

    private void MakeMoveLocal(int number)
    {
        MakeMove(number);
        m_board.MainGamer.Turn(number);
    }

    private void MakeMove(int number)
    {
        int i = number / 9;
        int j = number % 9 / 3;
        int k = number % 3;
        MakeMove(i, j, k);
    }

    private void MakeMove(int i, int j, int k)
    {
        GameObject o = m_cells[i,j,k];
        if (m_board.MainGamer.Symbol == ETicOrTac.X)
        {
            Instantiate(GameManager.instance.XPrefab, o.transform.position, Quaternion.identity, o.transform);
        }
        else
        {
            Instantiate(GameManager3D.instance.OPrefab, o.transform.position, Quaternion.identity, o.transform);
        }
    }


    public void Turn()
    {
        m_Turn = true;
    }

    public void EndTurn()
    {
        m_Turn = false;
    }

    public override void NewGame()
    {
        foreach(GameObject cell in m_cells)
        {
            if(cell != null)
            for(int i = cell.transform.childCount - 1; i >= 0; i--)
            {
                Destroy(cell.transform.GetChild(i).gameObject);
            }
        }
        m_Turn = false;
        AGamer firstGamer = new LocalGamer();
        firstGamer.StartTurn += Turn;
        firstGamer.EndTurn += EndTurn;
        AGamer secondGamer;

        if (computerMode)
        {
            secondGamer = new Computer();
            secondGamer.MakeTurn += MakeMove;
        }
        else
        {
            secondGamer = new LocalGamer();
            secondGamer.StartTurn += Turn;
            secondGamer.EndTurn += EndTurn;
        }
        m_board = new Board3D(firstGamer, secondGamer);
        m_board.WinEventWithWinLine += IsGameOver;
        m_GameRun = true;
        GameManager.instance.NewGame();
    }

    // Start is called before the first frame update
    void Start()
    {
        m_cells = new GameObject[3, 3, 3];
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    if (i == 1 && j == 1 && k == 1)
                    {
                        m_cells[i, j, k] = null;
                        continue;
                    }
                    GameObject cell = Instantiate(cellPrafab, GetCubePosition(i, j, k), Quaternion.identity, cellParent.transform);
                    cell.name = cellsName + i + j + k;
                    m_cells[i, j, k] = cell;
                }
            }
        }
        GameManager.instance.ChooseGamePlay(this);
        //NewGame();
    }

    private Vector3 GetCubePosition(int i, int j, int k)
    {
        Vector3 position = Vector3.zero;
        if (i < 1)
        {
            position.y = -distanceBetweenCells;
        }
        else
        {
            if (i > 1)
            {
                position.y = distanceBetweenCells;
            }
        }
        if (j < 1)
        {
            position.x = -distanceBetweenCells;
        }
        else
        {
            if (j > 1)
            {
                position.x = distanceBetweenCells;
            }
        }
        if (k < 1)
        {
            position.z = -distanceBetweenCells;
        }
        else
        {
            if (k > 1)
            {
                position.z = distanceBetweenCells;
            }
        }
        return position;
    }

    private void IsGameOver(ETicOrTac whoWin, List<int> winLine)
    {
        //ETicOrTac? whoWin = simpleTicTacToe.WhoWin();
        if (!m_GameRun)
            return;
        switch (whoWin)
        {
            case ETicOrTac.X:
                GameManager.instance.Win();
                //ShowWin(winLine.ToArray());
                GameOver();
                break;
            case ETicOrTac.O:
                GameManager.instance.Lose();
                //ShowWin(winLine.ToArray());
                GameOver();
                break;
            case ETicOrTac.Draw:
                GameManager.instance.Draw();
                GameOver();
                break;
        }

    }

    private void GameOver()
    {
        m_GameRun = false;
    }

    private void ChangeSpaceBetweenCells()
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < 3; k++)
                {

                    m_cells[i, j, k].transform.position = GetCubePosition(i,j,k);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("down");
            fp = Input.mousePosition;
            
            startTime = Time.time;
            cellParent.gameObject.transform.SetParent(cellObjectRotate.transform);
            //Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));
            //RaycastHit hit;
            //if(Physics.Raycast(ray,out hit, Mathf.Infinity)){
            //    Debug.Log("vhod1");
            //    ObjectClick(hit.transform.gameObject.name);
            //}
        }
        if (Input.GetMouseButtonUp(0))
        {
            float endTime = Time.time;
            Debug.Log("up");
            Debug.Log(endTime - startTime);
            Vector3 sp = Input.mousePosition;
            
            if(firstclick)
            if (endTime - startTime < differentTime && Mathf.Abs(fp.x - sp.x) < minDistanceBetweenTouch && Mathf.Abs(fp.y - sp.y) < minDistanceBetweenTouch)
            {

                Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1));
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    Debug.Log("vhod1");
                    ObjectClick(hit.transform.gameObject.name);
                }
                
            }
            else
            {

            }
            cellParent.gameObject.transform.SetParent(null);
            cellObjectRotate.transform.eulerAngles = Vector3.zero;
            Debug.Log("парент" + cellParent.transform.parent);
            if (canMakeFirstClick)
                firstclick = true;
        }
        if (Input.GetMouseButton(0))
        {
            Debug.Log("enter");
            Vector3 sp = Input.mousePosition;
            Debug.Log("y: " + -((fp.x - sp.x) / minDistanceBetweenTouch) + "  x: " + ((fp.y - sp.y) / minDistanceBetweenTouch));
            cellObjectRotate.transform.eulerAngles = new Vector3( -((fp.y - sp.y) / minDistanceBetweenTouch), ((fp.x - sp.x) / minDistanceBetweenTouch), 0);
        }
        
    }
}
