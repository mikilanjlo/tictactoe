﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gameplay2d : AGameplay
{
    [SerializeField] private GameObject m_lattice;
    //TicTacToe2d ticTacToe2d;
    private Board2D m_board2D;
    private GameObject[] m_bigCells;
    private GameObject[][] m_cells;


    private void Start()
    {
        m_bigCells = FindCells(m_lattice);
        List<GameObject[]> cells = new List<GameObject[]>();
        for (int i = 0; i < m_bigCells.Length; i++)
            cells.Add(FindCells((m_bigCells[i].gameObject.transform.Find(latticeName).gameObject)));
        m_cells = cells.ToArray();
        //ticTacToe2d = new TicTacToe2d();
        GameManager.instance.ChooseGamePlay(this);
        NewGame();
    }

    //private void SetActiveFalseLatticeChieldElement()
    //{

    //}

    private GameObject[] FindCells(GameObject lattice)
    {
        List<GameObject> cellsList = new List<GameObject>();
        for (int i = 1; i <= 9; i++)
            cellsList.Add(lattice.gameObject.transform.Find(cellsName + i).gameObject);
        return cellsList.ToArray();
    }


    public void Turn()
    {
        m_Turn = true;
    }

    public void EndTurn()
    {
        m_Turn = false;
    }

    public override void SetComputerMode(bool on)
    {
        base.SetComputerMode(on);
        NewGame();
    }


    public override void NewGame()
    {
        foreach (GameObject cell in m_bigCells)
        {
            cell.transform.Find("CellBack").gameObject.SetActive(false);
            cell.transform.Find("mid").gameObject.SetActive(false);
            cell.transform.Find("Panel").gameObject.SetActive(false);
            cell.transform.Find(latticeName).gameObject.SetActive(true);
        }
        for(int i = 0; i < m_cells.Length; i++)
            foreach (GameObject cell in m_cells[i])
            {
                cell.transform.Find("CellBack").gameObject.SetActive(false);
                cell.transform.Find("mid").gameObject.SetActive(false);
            }
        //ticTacToe2d.CreateNewGame();
        m_Turn = false;
        AGamer firstGamer = new LocalGamer();
        firstGamer.StartTurn += Turn;
        firstGamer.EndTurn += EndTurn;
        AGamer secondGamer;

        if (computerMode)
        {
            secondGamer = new Computer();
            secondGamer.MakeTurn += MakeMove;
        }
        else
        {
            secondGamer = new LocalGamer();
            secondGamer.StartTurn += Turn;
            secondGamer.EndTurn += EndTurn;
        }
        m_board2D = new Board2D(firstGamer,secondGamer);
        m_board2D.WinEventWithWinLine += IsGameOver;
        m_board2D.BigTurn += MakeBigMove;
        //m_computerTurn = false;
        m_GameRun = true;
        GameManager.instance.NewGame();
    }

    public override void ButtonClick(int bigCell, int cell)
    {
        if (!m_computerTurn && m_GameRun)
        {
            int ind = cell;
            Debug.Log(bigCell);
            if (m_board2D.IsFreePlace((bigCell-1)*9+(cell - 1)))
            {
                MakeMoveLocal(bigCell - 1,ind - 1);
            }

        }
    }

    private void ShowWin(int[] numbers)
    {
        foreach (int number in numbers)
        {
            GameObject o = null;
            string name = cellsName + number;
            foreach (GameObject cell in m_bigCells)
                if (cell.name == name)
                    o = cell;
            o.transform.Find("CellBack").gameObject.SetActive(true);
        }
    }

    //private IEnumerator ComputerTurn()
    //{
    //    yield return new WaitForSeconds(0.3f);
    //    //int number = ;
    //    (int bigCell, int cell) cells = ticTacToe2d.ComputerTurn();
    //    MakeMove(cells.bigCell,cells.cell);
    //    IsGameOver();
    //    m_computerTurn = false;
    //}

    private void MakeMoveLocal(int bigCell, int number)
    {
        MakeMove((bigCell /*- 1*/) * 9 + number /*- 1*/);
        //simpleBoard.MainGamer.Turn(number - 1);
        m_board2D.MainGamer.Turn(bigCell * 9 + number );
        ClosePanels();
    }

    private void MakeMove(int number)
    {
        
        GameObject o = null;
        string name = cellsName + (number  % 9 + 1);
        //Debug.Log("makemove" +bigCell + "  " + number);
        foreach (GameObject cell in m_cells[number / 9])
            if (cell.name == name)
                o = cell;
        Transform sprite = o.transform.Find("mid");
        if (m_board2D.MainGamer.Symbol == ETicOrTac.X)
        {
            sprite.gameObject.GetComponent<Image>().sprite = GameManager.instance.XSprite;
        }
        else
        {
            if (m_board2D.MainGamer.Symbol == ETicOrTac.O)
                sprite.gameObject.GetComponent<Image>().sprite = GameManager.instance.OSprite;
        }
        sprite.gameObject.SetActive(true);
        
        //ticTacToe2d.TakeThePlace(bigCell, number);
        //if (ticTacToe2d.GetSymbolXOBigCell(number) == null)
        //    ClosePanels(number);
        //else
        //    OpenAllPanels();
        //int? ind = ticTacToe2d.bigTurn;
        //if (ind != null)
        //    MakeBigMove((int)ind);
    }

    private void MakeBigMove(int number,ETicOrTac symbol)
    {
        GameObject o = null;
        string name = cellsName + (number + 1) ;
        foreach (GameObject cell in m_bigCells)
            if (cell.name == name)
                o = cell;
        o.transform.Find(latticeName).gameObject.SetActive(false);
        Transform sprite = o.transform.Find("mid");
        if (symbol == ETicOrTac.X)
        {
            sprite.gameObject.GetComponent<Image>().sprite = GameManager.instance.XSprite;
        }
        else
        {
            if (symbol == ETicOrTac.O)
            {
                sprite.gameObject.GetComponent<Image>().sprite = GameManager.instance.OSprite;
            }
            else
            {
                if (symbol == ETicOrTac.Draw)
                {
                    sprite.gameObject.GetComponent<Image>().sprite = GameSettings.instance.drawText;
                }
            }
        }
        sprite.gameObject.SetActive(true);
    }

    private void OpenAllPanels()
    {
        foreach (GameObject cell in m_bigCells)
        {
            cell.transform.Find("Panel").gameObject.SetActive(false);
        }
        
    }

    private void ClosePanels()
    {
        int ind = 0;
        List<bool> closeCells = m_board2D.CloseCells;
        foreach (GameObject cell in m_bigCells)
        {
            cell.transform.Find("Panel").gameObject.SetActive(closeCells[ind]);
                ind++;

        }
        //foreach (int cell in m_board2D.CloseCells)
        //    m_bigCells[]
    }

    private void IsGameOver(ETicOrTac whoWin, List<int> winLine)
    {
        //ETicOrTac? whoWin = simpleTicTacToe.WhoWin();
        if (!m_GameRun)
            return;
        switch (whoWin)
        {
            case ETicOrTac.X:
                GameManager.instance.Win();
                ShowWin(winLine.ToArray());
                GameOver();
                break;
            case ETicOrTac.O:
                GameManager.instance.Lose();
                ShowWin(winLine.ToArray());
                GameOver();
                break;
            case ETicOrTac.Draw:
                GameManager.instance.Draw();
                GameOver();
                break;
        }

    }

    private void GameOver()
    {
        OpenAllPanels();
        m_GameRun = false;
    }
}
