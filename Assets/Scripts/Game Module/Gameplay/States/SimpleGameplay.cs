﻿using Assets.Scripts;
using Assets.Scripts.TicTacToeClasses;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpleGameplay : AGameplay
{
    [SerializeField] private Lattice lattice;
    //SimpleTicTacToe simpleTicTacToe;
    ABoard simpleBoard;
    private GameObject[] m_gameObjects;

    //public SimpleGameplay()
    //{
        
    //}
    

    private void Start()
    {
        Debug.Log("start");
        List<GameObject> gameObjects = new List<GameObject>();
        for (int i = 1; i <= 9; i++)
            gameObjects.Add(lattice.gameObject.transform.Find(cellsName + i).gameObject);
        m_gameObjects = gameObjects.ToArray();
        //simpleTicTacToe = new SimpleTicTacToe();
        
        GameManager.instance.ChooseGamePlay(this);
        NewGame();
    }

    public override void ButtonClick(int bigCell, int cell)
    {
        if (m_Turn && m_GameRun)
        {
            int ind = cell;
            if (simpleBoard.IsFreePlace(ind-1))
            {
                MakeMoveLocal(ind);
                //m_Turn = false;
                //if (computerMode)
                //{
                //    m_computerTurn = true;
                //}
                //IsGameOver();
                //if (computerMode && m_GameRun)
                //    StartCoroutine(ComputerTurn());
            }

        }
    }

    public void Turn()
    {
        m_Turn = true;
    }

    public void EndTurn()
    {
        m_Turn = false;
    }

    public override void SetComputerMode(bool on)
    {
        base.SetComputerMode(on);
        NewGame();
    }

    public override void NewGame()
    {
        foreach (GameObject cell in m_gameObjects)
        {
            cell.transform.Find("CellBack").gameObject.SetActive(false);
            cell.transform.Find("mid").gameObject.SetActive(false);
        }
        m_Turn = false;
        AGamer firstGamer = new LocalGamer();
        firstGamer.StartTurn += Turn;
        firstGamer.EndTurn += EndTurn;
        AGamer secondGamer;
        if (computerMode)
        {
            secondGamer = new Computer();
            secondGamer.MakeTurn += MakeMove;
        }
        else
        {
            secondGamer = new LocalGamer();
            secondGamer.StartTurn += Turn;
            secondGamer.EndTurn += EndTurn;
        }
        
        simpleBoard = new SimpleBoard(firstGamer, secondGamer);
        simpleBoard.WinEventWithWinLine += IsGameOver;
        //simpleTicTacToe.CreateNewGame();
        //m_computerTurn = false;
        //XO = ETicOrTac.X;
        
        m_GameRun = true;
        GameManager.instance.NewGame();
    }



    private void MakeMoveLocal(int number)
    {
        MakeMove(number - 1);
        simpleBoard.MainGamer.Turn(number - 1);
    }

    private void MakeMove(int number)
    {
        //if (!m_GameRun)
        //    return;
        number++;
        GameObject o = null;
        string name = cellsName + number;
        foreach (GameObject cell in m_gameObjects)
            if (cell.name == name)
                o = cell;
        Transform sprite = o.transform.Find("mid");
        if (simpleBoard.MainGamer.Symbol == ETicOrTac.X)
            sprite.gameObject.GetComponent<Image>().sprite = GameManager.instance.XSprite;
        else
            sprite.gameObject.GetComponent<Image>().sprite = GameManager.instance.OSprite;
        sprite.gameObject.SetActive(true);
    }

    private void ShowWin(int[] numbers)
    {
        foreach (int number in numbers)
        {
            GameObject o = null;
            string name = cellsName + number;
            foreach (GameObject cell in m_gameObjects)
                if (cell.name == name)
                    o = cell;
            o.transform.Find("CellBack").gameObject.SetActive(true);
        }
    }



    //private IEnumerator ComputerTurn()
    //{
    //    yield return new WaitForSeconds(0.3f);
    //    int number = simpleTicTacToe.ComputerTurn();
    //    Debug.Log(number);
    //    MakeMove(number);
    //    IsGameOver();
    //    m_computerTurn = false;

    //}



    private void IsGameOver(ETicOrTac whoWin, List<int> winLine)
    {
        //ETicOrTac? whoWin = simpleTicTacToe.WhoWin();
        if (!m_GameRun)
            return;
        switch (whoWin)
        {
            case ETicOrTac.X:
                GameManager.instance.Win();
                ShowWin(winLine.ToArray());
                GameOver();
                break;
            case ETicOrTac.O:
                GameManager.instance.Lose();
                ShowWin(winLine.ToArray());
                GameOver();
                break;
            case ETicOrTac.Draw:
                GameManager.instance.Draw();
                GameOver();
                break;
        }
        
    }

    //private void WinOrLose(ETicOrTac whoWin)
    //{
    //    if(whoWin == simpleBoard.MainGamer.Symbol)
    //    {
    //        GameManager.instance.Win();
    //    }
    //    else
    //    {

    //    }
    //}

    private void GameOver()
    {
        m_GameRun = false;
    }
}
