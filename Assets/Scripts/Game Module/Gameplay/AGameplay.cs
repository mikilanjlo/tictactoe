﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AGameplay : MonoBehaviour
{
    [SerializeField] protected bool computerMode = true;
    protected static string cellsName = "cell";
    protected static string latticeName = "Lattice";
    protected bool m_computerTurn;
    protected bool m_Turn;
    protected bool m_GameRun;

    //protected Sprite XSprite;
    //protected Sprite OSprite;

    

    public abstract void ButtonClick(int bigCell , int cell);

    public abstract void NewGame();

    public virtual void SetComputerMode(bool on)
    {
        computerMode = on;
        GameManager.instance.SetComputerMode();
        //gameSet.SetActive(false);
    }

}
