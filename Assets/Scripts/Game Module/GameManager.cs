﻿using Assets.Scripts;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    #region Fields
    public Sprite XSprite { get; private set; }
    public Sprite OSprite { get; private set; }
    public GameObject XPrefab { get; private set; }
    public GameObject OPrefab { get; private set; }
    private GameObject gameSet;
    private GameObject gameOverText;
    private Sprite winText;
    private Sprite loseText;
    private Sprite drawText;
    //private GameObject musicManager;
    private bool settingsSet = false;

    public static GameManager instance;
    private AGameplay m_gameplay;
    #endregion

    #region Unity Methods

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance == this)
                Destroy(gameObject);
        }

        //DontDestroyOnLoad(gameObject);
    }

    //private void Start()
    //{
    //    //simpleTicTacToe = new SimpleTicTacToe();
    //    //NewGame();
    //}
    #endregion

    #region Public Methods
    public virtual void NewGame()
    {
        SettingsIsSet();
        gameOverText.SetActive(false);
        MusicManager.instance.Play(false);
       // musicManager.GetComponent<AudioSource>().Play();
    }

    public void SetComputerMode()
    {
        
        //m_gameplay.SetComputerMode(on);
        gameSet.SetActive(false);
    }

    public void ButtonClick(int bigCell,int cell)
    {
        m_gameplay.ButtonClick(bigCell, cell);
    }

    public void ChooseGamePlay(AGameplay gameplay)
    {
        Debug.Log("set");
        m_gameplay = gameplay;
    }

    public virtual void SetGameSettings(GameSettings gameSettings)
    {
        if (settingsSet)
            return;
        XPrefab = gameSettings.XPrefab;
        OPrefab = gameSettings.OPrefab;
        XSprite = gameSettings.XSprite;
        OSprite = gameSettings.OSprite;
        gameSet = gameSettings.gameSet;
        gameOverText = gameSettings.gameOverText;
        winText = gameSettings.winText;
        loseText = gameSettings.loseText;
        drawText = gameSettings.drawText;
        //musicManager = gameSettings.musicManager;
        settingsSet = true;
    }

    #endregion

    #region Private Methods

    private void SettingsIsSet()
    {
        GameSettings.instance.SetSettings();
    }
    

    public void Win()
    {
        GameOver(winText);
    }

    public void Lose()
    {
        GameOver(loseText);
    }

    public void Draw()
    {
        GameOver(drawText);
    }

    private void GameOver(Sprite sptiteGOText)
    {
        gameOverText.gameObject.GetComponent<Image>().sprite = sptiteGOText;
        MusicManager.instance.gameObject.GetComponent<AudioSource>().Stop();
        gameOverText.SetActive(true);
    }

    #endregion

}
