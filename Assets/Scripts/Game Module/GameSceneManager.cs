﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour
{
    public static GameSceneManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void LoadSampleGameScene()
    {
        SceneManager.LoadScene("SampleGameScene");
    }

    public void Load2DGameScene()
    {
        SceneManager.LoadScene("2dGameScene");
    }

    public void Load3DGameScene()
    {
        SceneManager.LoadScene("3dGameScene");
    }

    public void LoadPreMenuScene()
    {
        SceneManager.LoadScene("PreMenu");
    }

    public void LoadStartScene()
    {
        SceneManager.LoadScene("StartScene");
    }


    public void ExitGame()
    {
        Application.Quit();
    }
}
