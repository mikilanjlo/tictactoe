﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    [SerializeField] public Sprite XSprite;
    [SerializeField] public Sprite OSprite;
    [SerializeField] public GameObject XPrefab;
    [SerializeField] public GameObject OPrefab;
    [Header("Game Setting")]
    [SerializeField] public GameObject gameSet;
    [Header("Game Over Text")]
    [SerializeField] public GameObject gameOverText;
    [SerializeField] public Sprite winText;
    [SerializeField] public Sprite loseText;
    [SerializeField] public Sprite drawText;
    [Header("Music")]
    [SerializeField] public GameObject musicManager;

    public static GameSettings instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance == this)
                Destroy(gameObject);
        }

    }

    private void Start()
    {
        SetSettings();
        //musicManager.gameObject.GetComponent<AudioSource>().mute = gameSet.
    }

    public void SetSettings()
    {
        GameManager.instance.SetGameSettings(instance);
    }
}
