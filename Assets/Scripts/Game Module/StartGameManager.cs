﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGameManager : MonoBehaviour
{
    [SerializeField] GameObject settingPanel;
    [SerializeField] GameObject SoundButton;
    [SerializeField] Sprite SoundOn;
    [SerializeField] Sprite SoundOff;
    [SerializeField] AudioClip clip;
    //[SerializeField] MusicManager musicManager;

    public void Start()
    {
        MusicManager.instance.Play(true);
        ButtonSoundClick();
    }

    public void ButtonSettingClick()
    {
        settingPanel.SetActive(true);
    }

    public void ButtonSettingReturnClick()
    {
        settingPanel.SetActive(false);
    }

    public void ButtonSettingPrivatePolicyClick()
    {
        Application.OpenURL("http://mikilanjlo.wixsite.com/noughtsandcrosses");
    }

    public void ButtonSoundClick()
    {
        if (MusicManager.instance.IsMute())
            SoundButton.GetComponent<Image>().sprite = SoundOff;
        else
            SoundButton.GetComponent<Image>().sprite = SoundOn;
    }

}
