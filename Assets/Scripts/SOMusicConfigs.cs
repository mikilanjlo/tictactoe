﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="NewMusicConfig",menuName = "Music/MusicCfg")]
public class SOMusicConfigs : ScriptableObject
{
    [SerializeField] public bool mute;
    [SerializeField] public List<AudioClip> fightMusics;
    [SerializeField] public AudioClip menuMusic;
}
