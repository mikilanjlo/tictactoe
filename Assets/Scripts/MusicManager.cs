﻿using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField] SOMusicConfigs musicConfig;
    public static MusicManager instance;
    private AudioSource audioSource;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            if (instance == this)
                Destroy(gameObject);
        }
        gameObject.GetComponent<AudioSource>().mute = musicConfig.mute;
        audioSource = gameObject.GetComponent<AudioSource>();
        DontDestroyOnLoad(gameObject);
    }

    //private void Start()
    //{
    //    gameObject.GetComponent<AudioSource>().mute = musicConfig.mute;
    //}

    public void MuteOrOnMusic()
    {
        bool mute = gameObject.GetComponent<AudioSource>().mute;
        gameObject.GetComponent<AudioSource>().mute = !mute;
        musicConfig.mute = !mute;
    }

    public void SetSound()
    {
        gameObject.GetComponent<AudioSource>().mute = musicConfig.mute;
    }

    public bool IsMute()
    {
        return musicConfig.mute;
    }

    public void Play(bool isMenu)
    {
        //AudioSource audioSource = gameObject.GetComponent<AudioSource>();
        audioSource.Stop();
        if (!isMenu)
        {
            audioSource.clip = musicConfig.fightMusics[Random.Range(0, musicConfig.fightMusics.Count)];
        }
        else
        {
            audioSource.clip = musicConfig.menuMusic;
            //audioSource.loop = true;
        }
        audioSource.Play();

    }

    public void Stop()
    {
        audioSource.Stop();
    }
}
