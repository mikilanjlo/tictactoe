﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class ETicOrTacExtensions
{
    public static ETicOrTac InverseETicOrTac(this ETicOrTac symbol)
    {
        return symbol == ETicOrTac.O ? ETicOrTac.X : ETicOrTac.O;
    }

    public static ETicOrTac InverseETicOrTac(this ETicOrTac? symbol)
    {
        return symbol == ETicOrTac.O ? ETicOrTac.X : ETicOrTac.O;
    }
}

