﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//namespace Assets.Scripts.Extensions
//{
public static class ArrayExtensions
{
    public static ETicOrTac?[,] Copy(this ETicOrTac?[,] array)
    {
        int rows = array.GetUpperBound(0) + 1;
        int columns = array.Length / rows;
        ETicOrTac?[,] newarray = new ETicOrTac?[rows, columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
                newarray[i, j] = array[i, j];
        return newarray;
    }

    public static ETicOrTac?[,,] Copy(this ETicOrTac?[,,] array)
    {
        
        ETicOrTac?[,,] newarray = new ETicOrTac?[array.GetLength(0), array.GetLength(1), array.GetLength(2)];
        for (int i = 0; i < array.GetLength(0); i++)
            for (int j = 0; j < array.GetLength(1); j++)
                for (int k = 0; k < array.GetLength(2); k++)
                    newarray[i, j , k] = array[i, j , k];
        return newarray;
    }
}
//}
