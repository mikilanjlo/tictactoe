﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorExtensions 
{
    public static Vector3 Mul(this Vector3 vector, float number)
    {
        return new Vector3(vector.x * number, vector.y * number, vector.z * number);
    }

    public static Vector3 Mul(this Vector3 vector, int number)
    {
        return new Vector3(vector.x * number, vector.y * number, vector.z * number);
    }
}
