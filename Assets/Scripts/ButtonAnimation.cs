﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAnimation 
{
    public void ClickButtonAnim(GameObject button)
    {
        Vector3 scale = button.transform.localScale;
        button.transform.localScale = scale.Mul(0.9f);
    }
}
